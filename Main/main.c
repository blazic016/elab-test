/*
 * Copyright 2018 WolkAbout Technology s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "wolk_connector.h"
#include "wolk_utils.h"

#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <openssl/ssl.h>
#include <stdarg.h>
#include <sys/timeb.h>

static SSL_CTX* ctx;
static BIO* sockfd;

static const char* device_key = "yez3768ir8yheemq";
static const char* device_password = "489a61d8-aeb9-468c-b7b2-adfa67a455e6";
static const char* hostname = "api-demo.wolkabout.com";
static int portno = 8883;
static char certs[] = "../ca.crt";

/* Sample in-memory persistence storage - size 1MB */
static uint8_t persistence_storage[1024 * 1024];

/* WolkConnect-C Connector context */
static wolk_ctx_t wolk;

static volatile bool keep_running = true;


#define PATH_MAX 255
FILE *popenHandler(const char *args, const char *method) {
    return popen(args, method);
}

int pcloseHandler(FILE *fp) {
    return pclose(fp);
}
        
char *saveTerminalCommand(char str[]) {
    FILE *fp;            
    char path[PATH_MAX]; 

    fp = popenHandler(str, "r"); 
    if (fp == NULL) {
        printf("ERROR: Failed to execute command %s\n", str);
    }

    char *full =malloc(1);
    if ( full == NULL ){ 
        printf("allocation failed\n");
    }

    char *temp;
    while (fgets(path, PATH_MAX, fp) != NULL) {
        temp=realloc(full,(strlen(full)+1)+(strlen(path)+1));
        if ( temp == NULL ) {
                printf("allocation failed\n");
            break;
        }
        full=temp;
        strcat(full,path);
    }
    pcloseHandler(fp);
    return full; 
}

char *strstrtok(char *src, char *sep) {
    static char *str = NULL;
    if (src) str = src;
    else src = str;

    if (str == NULL)
        return NULL;

    char *next = strstr(str, sep);
    if (next) {
        *next = '\0';
        str = next + strlen(sep);
    }
    else
        str = NULL;

    return src;
}

char *splitBetweenString(char *full_string, char *from, char *to) {
    char *token = strstrtok(full_string, from);
     char *arr_str[3] = {};
     int index = 0;
    while (token) {   
        arr_str[index] = token;
        token = strstrtok(NULL, to);
        index++;
    }
    return arr_str[1];
}

static void int_handler(int dummy) {
    WOLK_UNUSED(dummy);
    keep_running = false;
}

static int send_buffer(unsigned char* buffer, unsigned int len) {
    int n;
    n = (int)BIO_write(sockfd, buffer, (int)len);
    if (n < 0) {
        return -1;
    }
    return n;
}

static int receive_buffer(unsigned char* buffer, unsigned int max_bytes)
{
    bzero(buffer, max_bytes);
    int n;
    n = (int)BIO_read(sockfd, buffer, (int)max_bytes);
    if (n < 0) {
        return -1;
    }
    return n;
}

static void open_socket(BIO** bio, SSL_CTX** ssl_ctx, const char* addr, const int port_number, const char* ca_file,
                        const char* ca_path)
{
    SSL* ssl;
    char port[5];
    snprintf(port, 5, "%d", port_number);

    /* Load OpenSSL */
    SSL_load_error_strings();
    ERR_load_BIO_strings();
    OpenSSL_add_all_algorithms();
    SSL_library_init();

    *ssl_ctx = SSL_CTX_new(SSLv23_client_method());

    /* Load Certificate */
    if (!SSL_CTX_load_verify_locations(*ssl_ctx, ca_file, ca_path)) {
        printf("Wolk client - Error, failed to load certificate\n");
        exit(1);
    }

    /* Open BIO Socket */
    *bio = BIO_new_ssl_connect(*ssl_ctx);
    BIO_get_ssl(*bio, &ssl);
    SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);
    BIO_set_conn_hostname(*bio, addr);
    BIO_set_nbio(*bio, 1);
    BIO_set_conn_port(*bio, port);

    /* Wait for connection in 15 second timeout */
    int start_time = (int)time(NULL);
    while (BIO_do_connect(*bio) <= 0 && (int)time(NULL) - start_time < 15)
        ;
    if (BIO_do_connect(*bio) <= 0) {
        printf("Wolk client - Error, do connect\n");
        BIO_free_all(*bio);
        SSL_CTX_free(*ssl_ctx);
        *bio = NULL;
        *ssl_ctx = NULL;
        return;
    }

    /* Verify Certificate */
    if (SSL_get_verify_result(ssl) != X509_V_OK) {
        printf("Wolk client - Error, x509 certificate verification failed\n");
        exit(1);
    }
}

static char device_configuration_references[CONFIGURATION_ITEMS_SIZE][CONFIGURATION_REFERENCE_SIZE] = {"LOG_LEVEL"};
static char device_configuration_values[CONFIGURATION_ITEMS_SIZE][CONFIGURATION_VALUE_SIZE] = {"INFO"};

size_t num_configuration_items= 1;
static void configuration_handler(char (*reference)[CONFIGURATION_REFERENCE_SIZE],
                                  char (*value)[CONFIGURATION_VALUE_SIZE], size_t num_configuration_items)
{
    for (size_t i = 0; i < num_configuration_items; ++i) {
        //printf("Promena iz platforme - Reference: %s | Value: %s\n", reference[i], value[i]);

        strcpy(device_configuration_references[i], reference[i]);
        strcpy(device_configuration_values[i], value[i]);
    }
}

char log_listener[255];
static size_t configuration_provider(char (*reference)[CONFIGURATION_REFERENCE_SIZE],
                                     char (*value)[CONFIGURATION_VALUE_SIZE], size_t max_num_configuration_items)
{
    WOLK_UNUSED(max_num_configuration_items);
    WOLK_ASSERT(max_num_configuration_items >= NUMBER_OF_CONFIGURATION);

    for (size_t i = 0; i < CONFIGURATION_ITEMS_SIZE; ++i) {
        strcpy(reference[i], device_configuration_references[i]);
        strcpy(value[i], device_configuration_values[i]);
        printf("Configuration: Reference: %s | Value: %s\n", reference[i], value[i]);
    }
    strcpy(log_listener, value[0]);
    //printf("testiraj: %s\n", log_listener);
    
    return CONFIGURATION_ITEMS_SIZE;
}

int is_log_debug = 0;
int is_log_info = 1;
#define LOG_DEBUG 0
#define LOG_INFO 1

void MyLog(int LOG_level, char* format, ...)
{
	static char msg_buf[256];
	va_list args;
	struct timeb ts;
	struct tm *timeinfo;
	ftime(&ts);
	timeinfo = localtime(&ts.time);
	strftime(msg_buf, 80, "%Y/%m/%d %H:%M:%S ", timeinfo);
    
    FILE *fp;
    fp = fopen("logging.txt", "a"); 
    if(fp == NULL) {
        printf("Error opening a logging.txt file");
    }

	if (LOG_level == LOG_DEBUG && is_log_debug == 1) {
		va_start(args, format);
		vsnprintf(&msg_buf[strlen(msg_buf)], sizeof(msg_buf) - strlen(msg_buf), format, args);
		va_end(args);
        fprintf(fp, "%s\n", msg_buf);
	}

	if (LOG_level == LOG_INFO && is_log_info == 1) {
		va_start(args, format);
		vsnprintf(&msg_buf[strlen(msg_buf)], sizeof(msg_buf) - strlen(msg_buf), format, args);
		va_end(args);
        fprintf(fp, "%s\n", msg_buf);
	}
    fflush(stdout);
    fclose(fp);
    return;
}


int main(int argc, char* argv[])
{
    WOLK_UNUSED(argc);
    WOLK_UNUSED(argv);

    signal(SIGINT, int_handler);
    signal(SIGTERM, int_handler);

    if (strcmp(device_key, "device_key") == 0) { /* strcmp == 0 - compare strings wether equal */
        printf("Wolk client - Error, device key not provided\n");
        return 1;
    }

    if (strcmp(device_password, "password") == 0) {
        printf("Wolk client - Error, password key not provided\n");
        return 1;
    }

    printf("Wolk client - Establishing connection to WolkAbout IoT platform\n");
    open_socket(&sockfd, &ctx, hostname, portno, certs, NULL);
    if (sockfd == NULL) {
        printf("Wolk client - Error establishing connection to WolkAbout IoT platform\n");
        return 1;
    }

    if (wolk_init(&wolk, send_buffer, receive_buffer, NULL, NULL, configuration_handler, configuration_provider, device_key,
                  device_password, PROTOCOL_JSON_SINGLE, NULL, 0)
        != W_FALSE) {
        printf("Error initializing WolkConnect-C\n");
        return 1;
    }

    if (wolk_init_in_memory_persistence(&wolk, persistence_storage, sizeof(persistence_storage), false) != W_FALSE) {
        printf("Error initializing in-memory persistence\n");
        return 1;
    }

    printf("Wolk client - Connecting to server\n");
    if (wolk_connect(&wolk) != W_FALSE) {
        printf("Wolk client - Error connecting to server\n");
        return -1;
    }
    printf("Wolk client - Connected to server\n");



    char IP_init[15];
    int cnt = 0;
    double tmp_array[5] = {};

    FILE *file = popen("host myip.opendns.com resolver1.opendns.com | grep \"myip.opendns.com has\" | awk '{print $4}'", "r");
    if (file) {
        fgets(IP_init, sizeof IP_init, file);
        pclose(file);
    }

    char IP_init2[strlen(IP_init)];
    file = popen("host myip.opendns.com resolver1.opendns.com | grep \"myip.opendns.com has\" | awk '{print $4}'", "r");
    if (file) {
        fgets(IP_init2, sizeof IP_init2, file);
        MyLog(LOG_INFO, "Init IP: %s", IP_init2);
        printf("Init IP: %s\n", IP_init2);
        pclose(file);
    }

    wolk_add_string_sensor_reading(&wolk, "IP_ADD", IP_init2, 0);

    while (keep_running) {
        if (strcmp(log_listener, "INFO") == 0) {
            is_log_info = 1;
            is_log_debug = 0;
        } else if (strcmp(log_listener, "DEBUG") == 0) {
            is_log_info = 0;
            is_log_debug = 1;
        } else {
            is_log_info = 1;
            is_log_debug = 0;
        }
        wolk_process(&wolk, 1);

        /* 1 min */
            char *str_terminal = saveTerminalCommand("/opt/vc/bin/vcgencmd measure_temp"); /* Raspberry Pi */
            char *tmp_str = splitBetweenString(str_terminal, "p=", "'");
            double tmp_cur = (double) atoll(tmp_str);
            MyLog(LOG_DEBUG, "Float CPU: %.2f" ,tmp_cur); 
            tmp_array[cnt] = tmp_cur;
            MyLog(LOG_INFO, "CPU minute #%d: %.2f" ,cnt+1, tmp_array[cnt]);
            printf("CPU minute #%d: %.2f\n" ,cnt+1, tmp_array[cnt]); 

	
        /* 5 min */
        if (cnt == 4) {
                double tmp_max = 0;
                for(int i=0; i<=cnt; i++) {
                    if (tmp_array[i]>tmp_max) {
                        tmp_max = tmp_array[i];
                    }
                }
                MyLog(LOG_INFO, "Max CPU: %.2f", tmp_max);
                printf("Max CPU: %.2f\n", tmp_max);
                wolk_add_numeric_sensor_reading(&wolk, "CPU_T", tmp_max, 0);
                wolk_publish(&wolk);

            /* Publish current IP if changed from initial IP */
            char IP_current[15];
            file = popen("host myip.opendns.com resolver1.opendns.com | grep \"myip.opendns.com has\" | awk '{print $4}'", "r");
            if (file) {
                fgets(IP_current, sizeof IP_current, file);
                pclose(file);
            }
            char IP_current2[strlen(IP_current)];
            file = popen("host myip.opendns.com resolver1.opendns.com | grep \"myip.opendns.com has\" | awk '{print $4}'", "r");
            if (file) {
                fgets(IP_current2, sizeof IP_current2, file);
                MyLog(LOG_INFO, "Current IP: %s", IP_current2);
                printf("Current IP: %s\n", IP_current2);
                pclose(file);
            }

            MyLog(LOG_DEBUG, "Compare IP: %d", strcmp(IP_init2, IP_current2));

            if (strcmp(IP_init2, IP_current2) == 0) {
                MyLog(LOG_INFO, "IP addresses are the same.");
            } else {
                MyLog(LOG_INFO, "Current IP address has changed from initial state.");
                printf("Current IP address has changed from initial state.\n");
                wolk_add_string_sensor_reading(&wolk, "IP_ADD", IP_current2, 0);
                wolk_publish(&wolk);
            }

            cnt = -1;
        }
        cnt++;
        sleep(60);
    }

    printf("Wolk client - Diconnecting\n");

    wolk_disconnect(&wolk);

    BIO_free_all(sockfd);

    return 0;
}
